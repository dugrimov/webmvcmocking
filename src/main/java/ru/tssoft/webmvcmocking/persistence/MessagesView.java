package ru.tssoft.webmvcmocking.persistence;

import java.util.List;

public interface MessagesView {
    List<Message> getMessages(Integer userId);
}

package ru.tssoft.webmvcmocking.persistence;

public interface MessagesRepository {
    Long createMessage(Message message);
}

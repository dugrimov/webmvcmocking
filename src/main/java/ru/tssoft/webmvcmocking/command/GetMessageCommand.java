package ru.tssoft.webmvcmocking.command;

import org.springframework.stereotype.Component;
import ru.tssoft.webmvcmocking.model.MessageModel;

@Component
public class GetMessageCommand {
    public MessageModel execute(Long messageId){
        throw new UnsupportedOperationException();
    }
}

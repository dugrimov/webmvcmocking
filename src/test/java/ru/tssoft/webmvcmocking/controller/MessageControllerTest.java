package ru.tssoft.webmvcmocking.controller;

import java.util.Arrays;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.tssoft.webmvcmocking.command.CreateMessageCommand;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import ru.tssoft.webmvcmocking.command.GetNewestMessagesCommand;
import ru.tssoft.webmvcmocking.model.MessageModel;

@RunWith(MockitoJUnitRunner.class)
public class MessageControllerTest {

    @Mock
    private CreateMessageCommand createMessageCommandMock;
    @Mock
    private GetNewestMessagesCommand getNewestMessagesCommandMock;
    @InjectMocks
    private MessageController controller;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testCreateMessage() throws Exception {
        // инициализация исходных данных
        MessageModel message = new MessageModel();
        message.setText("Привет!");
        message.setUserSenderId(1);
        message.setUserRecipientId(2);
        final Long messageId = 999l;
        // настройка мока команды createMessageCommand
        when(createMessageCommandMock.execute(anyString(), anyInt(), anyInt())).thenReturn(messageId);
        // эмулируем HTTP запрос к контроллеру
        mockMvc.perform(put("/message") // PUT по адресу /message
                .contentType(TestUtil.APPLICATION_JSON_UTF8) // настройка заголовка запроса
                .content(TestUtil.convertObjectToJsonBytes(message))) // контент - наше сообщение в формате JSON
                .andExpect(status().isCreated()) // ожидаем в ответ HTTP статус 201
                .andExpect(content().string(messageId.toString())); // в ответе должен быть идентификатор нового сообщения
        
        // проверяем, что контроллер вызвал команду с правильными аргументами
        verify(createMessageCommandMock).execute(message.getText(), message.getUserSenderId(), message.getUserRecipientId());
    }

    @Test
    public void testNewMessages() throws Exception {
        // инициализация исходных данных
        final Integer userId = 111;
        MessageModel message1 = new MessageModel();
        message1.setText("текст 1");
        message1.setUserRecipientId(userId);
        message1.setUserSenderId(222);
        MessageModel message2 = new MessageModel();
        message2.setText("текст 2");
        message2.setUserRecipientId(userId);
        message2.setUserSenderId(333);
        // настройка мока команды getNewestMessagesCommand
        when(getNewestMessagesCommandMock.execute(userId)).thenReturn(Arrays.asList(message1, message2));
        // эмулируем HTTP запрос к контроллеру
        mockMvc.perform(get("/message/{userId}", userId)) // GET по адресу /message/111
                .andExpect(status().isOk()) // ожидаем в ответ HTTP статус 200
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8)) // в ответе должен быть заголовок с типом контента
                // далее проверяем контент, который вернул контроллер
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].userRecipientId", is(message1.getUserRecipientId())))
                .andExpect(jsonPath("$[0].userSenderId", is(message1.getUserSenderId())))
                .andExpect(jsonPath("$[0].text", is(message1.getText())))
                .andExpect(jsonPath("$[1].userRecipientId", is(message2.getUserRecipientId())))
                .andExpect(jsonPath("$[1].userSenderId", is(message2.getUserSenderId())))
                .andExpect(jsonPath("$[1].text", is(message2.getText())));
        
        // проверяем, что контроллер вызвал команду с правильными аргументами
        verify(getNewestMessagesCommandMock).execute(userId);
    }
}

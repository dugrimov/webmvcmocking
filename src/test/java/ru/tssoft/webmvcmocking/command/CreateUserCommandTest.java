package ru.tssoft.webmvcmocking.command;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.argThat;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import ru.tssoft.webmvcmocking.model.UserModel;
import ru.tssoft.webmvcmocking.persistence.User;
import ru.tssoft.webmvcmocking.persistence.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class CreateUserCommandTest {

    @Mock // мок для UserRepository
    private UserRepository userRepositoryMock;
    @InjectMocks // тестируемый класс - в него будут внедрены моки вместо реальных объектов
    private CreateUserCommand command;

    @Test
    public void testExecute() {
        // создаем объект, который контроллер передаст команде
        final UserModel userModel = new UserModel();
        userModel.setName("Ivanov");
        userModel.setEmail("ivanov@test.com");
        Integer expectedUserId = 2;
        // в анонимном классе проверим сохраненный в БД объект
        when(userRepositoryMock.create(argThat(new ArgumentMatcher<User>() {
            @Override
            public boolean matches(Object argument) {
                User user = (User) argument;
                return user.getId() == null && user.getName().equals(userModel.getName())
                        && user.getEmail().equals(userModel.getEmail()) && user.getCreateDate() != null;
            }
        }))).thenReturn(expectedUserId);
        Integer actualUserId = command.execute(userModel);
        assertEquals(expectedUserId, actualUserId);
    }
}

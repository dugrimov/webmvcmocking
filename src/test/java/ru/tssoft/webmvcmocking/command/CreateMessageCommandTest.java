package ru.tssoft.webmvcmocking.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import ru.tssoft.webmvcmocking.persistence.Message;
import ru.tssoft.webmvcmocking.persistence.MessagesRepository;

@RunWith(MockitoJUnitRunner.class)
public class CreateMessageCommandTest {

    @Mock
    private MessagesRepository messagesRepositoryMock;
    @InjectMocks
    private CreateMessageCommand command;

    @Test
    public void testExecute() {
        // инициализация исходных данных для теста
        String expectedText = "Hello, world!";
        Integer expectedUserSenderId = 111;
        Integer expectedUserRecipientId = 222;
        Long expectedMessageId = 999l;
        // настройка перехватчика аргумента, который будет передан в мок
        ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
        when(messagesRepositoryMock.createMessage(messageCaptor.capture())).thenReturn(expectedMessageId);
        
        // вызываем тестируемый метод с исходными данными
        Long actualMessageId = command.execute(expectedText, expectedUserSenderId, expectedUserRecipientId);
        assertEquals(expectedMessageId, actualMessageId);
        // получаем аргумент, с которым был вызван метод messagesRepository.createMessage()
        Message actualMessage = messageCaptor.getValue();
        // проверяем, правильно ли команда собрала объект для сохранения в БД 
        assertEquals(expectedText, actualMessage.getText());
        assertEquals(expectedUserSenderId, actualMessage.getUserSenderId());
        assertEquals(expectedUserRecipientId, actualMessage.getUserRecipientId());
        assertNotNull(actualMessage.getMessageDate());
    }
}

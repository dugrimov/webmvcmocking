package ru.tssoft.webmvcmocking.command;

import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;
import ru.tssoft.webmvcmocking.model.MessageModel;
import ru.tssoft.webmvcmocking.persistence.Message;
import ru.tssoft.webmvcmocking.persistence.MessagesView;
import ru.tssoft.webmvcmocking.persistence.UserRepository;

// аннотация позволит Mockito проинициализировать моки внутри тестового класса
@RunWith(MockitoJUnitRunner.class)
public class GetNewestMessagesCommandTest {

    @Mock // мок для MessagesView
    private MessagesView messagesViewMock; 
    @Mock // мок для UserRepository
    private UserRepository userRepositoryMock;
    @InjectMocks // тестируемый класс - в него будут внедрены моки вместо реальных объектов
    private GetNewestMessagesCommand command;

    @Test
    public void testExecute() {
        // создадим сообщения, которые должен вернуть messagesViewMock
        final Integer userId = 1;
        final Message message1 = new Message();
        message1.setText("text 1");
        message1.setId(1l);
        message1.setUserSenderId(111);
        message1.setUserRecipientId(userId);
        final Message message2 = new Message();
        message2.setText("text 2");
        message2.setId(2l);
        message2.setUserSenderId(333);
        message2.setUserRecipientId(userId);
        // настраиваем mock для метода messagesView.getMessages(): 
        // если входной параметр равен userId, то вернуть список наших сообщений
        when(messagesViewMock.getMessages(userId)).thenReturn(Arrays.asList(message1, message2));

        // вызываем тестируемый метод, внутри происходит обращение к messagesViewMock и userRepositoryMock
        List<MessageModel> actualMessages = command.execute(userId);
        // проверяем результат работы команды
        assertEquals(2, actualMessages.size());
        assertEquals(message1.getText(), actualMessages.get(0).getText());
        assertEquals(message1.getUserSenderId(), actualMessages.get(0).getUserSenderId());
        assertEquals(message1.getUserRecipientId(), actualMessages.get(0).getUserRecipientId());
        assertEquals(message2.getText(), actualMessages.get(1).getText());
        assertEquals(message2.getUserSenderId(), actualMessages.get(1).getUserSenderId());
        assertEquals(message2.getUserRecipientId(), actualMessages.get(1).getUserRecipientId());
        // проверяем, что команда вызвала у userRepository нужный метод с правильными аргументами
        verify(userRepositoryMock).setLastViewedMessageId(userId, message2.getId());
    }

}
